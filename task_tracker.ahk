#SingleInstance force
#Persistent

SetKeyDelay, -1, 2
DetectHiddenWindows, On
DetectHiddenText, On

Global euid, timer_status, gui_x_pos, gui_y_pos, MainGuiHwnd, control_timer_status, control_task_name, control_task_comments, control_start_time, control_elapsed_time, db_file, week_day_table, control_movement_reminder, movement_reminder_status, movement_reminder_status_default, control_movement_reminder_elapsed, movement_timer

week_day_table := {Mon: 1, Tue: 2, Wed: 3, Thu: 4, Fri: 5, Sat: 6, Sun: 7}

elapsed_time = 0
timer_status = 0
movement_timer = 0
movement_reminder = 3600 ; One hour
movement_reminder_status_default = 1 ; On by default. Change to 0 to turn off
movement_reminder_status = %movement_reminder_status_default%

gui_x_pos = 557
gui_y_pos = 221

euid = %A_UserName% ;#Gets euid of currently logged on user#
StringLower, euid, euid

db_file = C:\Users\%euid%\Documents\Task Tracker\bin\files.txt

SetTimer, add_to_timer, 1000 ;# Every second #

SetTimer, check_for_running_timer, 30000 ;# Every 30 Seconds #

SetTimer, check_for_movement_reminder, 10000 ;# Every 30 Seconds #

create_gui("00 FF 00", "C0 C0 C0", "C0 C0 C0")
return

;;
; Creates the initial GUI
;;
create_gui(start_color, pause_color, finish_color)
{
  Gui, MainGui:+HwndMainGuiHwnd
  movement_reminder_checked := ""
  if movement_reminder_status = 1
  {
    movement_reminder_checked := "CheckedGray"
	output_reminder_time := format_seconds(movement_timer)
	Gui, MainGui:Add, Text, x152 y259 w80 h20 vcontrol_movement_reminder_elapsed, %output_reminder_time%
  }
  Gui, MainGui:+HwndMainGuiHwnd
  Gui, MainGui:font,s12
  Gui, MainGui:Add, Text, x72 y10 w60 h20 vcontrol_timer_status, Stopped
  Gui, MainGui:font
  Gui, MainGui:Add, Text, x12 y40 w80 h20, Task title:
  Gui, MainGui:Add, Edit, x12 y60 w210 h20 vcontrol_task_name,
  Gui, MainGui:Add, Text, x12 y90 w210 h20, Comments for task:
  Gui, MainGui:Add, Edit, x12 y110 w210 h90 vcontrol_task_comments,
  Gui_AddPicture("x232 y10 w110 h70", start_color) ; background
  Gui, MainGui:font,s10
  Gui, MainGui:Add, Text, xp yp wp hp gstart_timer BackgroundTrans Center 0x200 E0x200, Start
  Gui_AddPicture("x232 y100 w110 h70", pause_color) ; background
  Gui, MainGui:Add, Text, xp yp wp hp gpause_timer BackgroundTrans Center 0x200 E0x200, Pause
  Gui_AddPicture("x232 y180 w110 h70", finish_color) ; background
  Gui, MainGui:Add, Text, xp yp wp hp gfinish_timer BackgroundTrans Center 0x200 E0x200, Finish
  Gui, MainGui:font
  Gui, MainGui:Add, Text, x12 y210 w40 h20, Started:
  Gui, MainGui:Add, Text, x62 y210 w80 h20 vcontrol_start_time, %time_task_started%
  Gui, MainGui:Add, Text, x12 y240 w40 h20, Elapsed
  Gui, MainGui:Add, Text, x62 y240 w80 h20 vcontrol_elapsed_time, %time_task_elapsed%
  Gui, MainGui:font,s11
  Gui, MainGui:Add, Text, x12 y269 w100 h20 , Summaries:
  Gui, MainGui:font
  Gui, MainGui:Add, CheckBox, x152 y210 w80 h40 %movement_reminder_checked% vcontrol_movement_reminder, Movement Reminder
  Gui, MainGui:Add, Button, x12 y289 w50 h30 gsummarize_day, Day
  Gui, MainGui:Add, Button, x72 y289 w60 h30 gsummarize_week_by_day, Week by day
  Gui, MainGui:Add, Button, x142 y289 w60 h30 gsummarize_week, Week
  Gui, MainGui:Add, Button, x212 y289 w60 h30 gsummarize_month, Month
  Gui, MainGui:Add, Button, x282 y289 w60 h30 gsummarize_year, Year
  Gui, MainGui:Show, x%gui_x_pos% y%gui_y_pos% h328 w354, Task Tracker

  return
}

;;
; Prompts user if task is running, otherwise exits app
;;
MainGuiGuiClose:
  if (timer_status = 1 or timer_status = 2)
  {
    Msgbox, 4, Task Tracker Exit, There is a running timer, are you sure you would like to exit?
    ifMsgbox no
    {
      return
    }
  }
  ExitApp
return

;;
; Creates the month summary prompt
;;
create_month_gui()
{
  Global
  FormatTime, cur_month,, M

  Gui, MonthGui:Add, Text, x12 y9 w250 h20 , Enter year and select month (blank for current year)
  Gui, MonthGui:Add, Edit, x262 y9 w100 h20 vselected_year, 
  Gui, MonthGui:Add, Button, x12 y39 w60 h40 v1 gsummarize_selected_month, January
  Gui, MonthGui:Add, Button, x82 y39 w60 h40 v2 gsummarize_selected_month, February
  Gui, MonthGui:Add, Button, x152 y39 w60 h40 v3 gsummarize_selected_month, March
  Gui, MonthGui:Add, Button, x222 y39 w60 h40 v4 gsummarize_selected_month, April
  Gui, MonthGui:Add, Button, x292 y39 w60 h40 v5 gsummarize_selected_month, May
  Gui, MonthGui:Add, Button, x362 y39 w60 h40 v6 gsummarize_selected_month, June
  Gui, MonthGui:Add, Button, x12 y89 w60 h40 v7 gsummarize_selected_month, July
  Gui, MonthGui:Add, Button, x82 y89 w60 h40 v8 gsummarize_selected_month, August
  Gui, MonthGui:Add, Button, x152 y89 w60 h40 v9 gsummarize_selected_month, September
  Gui, MonthGui:Add, Button, x222 y89 w60 h40 v10 gsummarize_selected_month, October
  Gui, MonthGui:Add, Button, x292 y89 w60 h40 v11 gsummarize_selected_month, November
  Gui, MonthGui:Add, Button, x362 y89 w60 h40 v12 gsummarize_selected_month, December
  Gui, MonthGui:Add, Button, x185 y139 w60 h40 vcur gsummarize_selected_month, Current Month
  Gui, MonthGui:Show, x742 y411 h195 w434, Task Tracker Summarizer
  return
}

;;
; Destroys the month Gui when closed
;;
MonthGuiGuiClose:
  Gui, MonthGui:Destroy
return

;;
; Starts the timer and updates the GUI
;;
start_timer:
 if timer_status = 1
 {
  return
 }

  GuiControlGet, movement_reminder_status, MainGui:, control_movement_reminder
  GuiControlGet, task_name, MainGui:, control_task_name
  GuiControlGet, task_comments, MainGui:, control_task_comments
  if task_name =
  {
    Msgbox, 0, Task Tracker Error, Please Enter a task
    return
  }
  FormatTime, start_time,, hh:mm ;#Get time right now#
  timer_status = 1

  WinGetPos, gui_x_pos, gui_y_pos
  Gui, MainGui:Destroy
  create_gui("C0 C0 C0", "FF FF 00", "FF 00 00")
  WinWait, Task Tracker
  GuiControl, MainGui:, control_task_comments, %task_comments%
  GuiControl, MainGui:, control_task_name, %task_name%
  GuiControl, MainGui:, control_start_time, %start_time%
  GuiControl, MainGui:, control_timer_status, Running
  GuiControl, MainGui:Disable, control_task_name
return

;;
; Pauses the timer and updates the GUI
;;
pause_timer:
 if timer_status != 1
 {
   return
 }

  timer_status = 2

  GuiControlGet, task_comments, MainGui:, control_task_comments
  WinGetPos, gui_x_pos, gui_y_pos
  Gui, MainGui:Destroy
  create_gui("00 FF 00", "C0 C0 C0", "FF 00 00")
  WinWait, Task Tracker
  GuiControl, MainGui:, control_task_comments, %task_comments%
  ControlSend,control_task_comments,{End}foo, ahk_id %MainGuiHwnd%
  GuiControl, MainGui:, control_task_name, %task_name%
  GuiControl, MainGui:, control_timer_status, Paused
  GuiControl, MainGui:, control_start_time, %start_time%
  GuiControl, MainGui:Disable, control_task_name
  GuiControl, MainGui:, control_elapsed_time, %output_time%
return

;;
; Finishes the timer for the tasks and updates the GUI
;;
finish_timer:
  if timer_status = 0
  {
    return
  }

  timer_status = 0

  GuiControlGet, task_name, MainGui:, control_task_name
  GuiControlGet, task_comments, MainGui:, control_task_comments

  WinGetPos, gui_x_pos, gui_y_pos
  Gui, MainGui:Destroy
  create_gui("00 FF 00", "C0 C0 C0", "C0 C0 C0")
  WinWait, Task Tracker
  GuiControl, MainGui:, control_timer_status, Stopped
  GuiControl, MainGui:, control_start_time,
  GuiControl, MainGui:, control_elapsed_time,
  GuiControl, MainGui:, control_task_name,
  GuiControl, MainGui:, control_task_comments,
  GuiControl, MainGui:Enable, control_task_name

  append_time_to_task(task_name, task_comments, elapsed_time)
  elapsed_time = 0
return

;;
; Summarizes time for tasks over the day
;;
summarize_day:
  summarize_tasks(1, "day")
return

;;
; Summarizes time for tasks over the week
;;
summarize_week:
  FormatTime, day_week,,ddd
  num_day_week := week_day_table[day_week]
  summarize_tasks(num_day_week, "week")
return

;;
; Summarizes time for tasks over the week by day
;;
summarize_week_by_day:
  FormatTime, day_week,,ddd
  num_day_week := week_day_table[day_week]
  summarize_each_day_of_week(num_day_week, "week_by_day")
return

;;
; Summarizes time for tasks over the month
;;
summarize_month:
  create_month_gui()
return

;;
; Summarizes time for tasks over the year
;;
summarize_year:
  InputBox, selected_year, Task Tracker Summarizer, Enter the year to summarize,,250,130
  if ErrorLevel
  {
    return
  }
  if selected_year =
  {
    Msgbox, 0, Task Tracker, No year entered.
    return
  }
  
  summarize_user_selection("", selected_year, "year")
return

;;
; Summarizes the selected month 
;;
summarize_selected_month:
  GuiControlGet, selected_year, MonthGui:, selected_year
  GuiControlGet, selected_month, MonthGui:FocusV
  Gui, MonthGui:Destroy
  
  FormatTime, cur_month,, M
  FormatTime, cur_year,, yyyy
  if(selected_month = "cur")
  {
    selected_month = %cur_month%
  }
  if(selected_year = "")
  {
    selected_year = %cur_year%
  }

  if(selected_year = cur_year AND selected_month = cur_month)
  {
    ; Summarize current month
    FormatTime, day_month,, d
    summarize_tasks(day_month, "month")
    return
  }
  
  summarize_user_selection(selected_month, selected_year, "month")
return

;;
; Increments timer and updates GUI
;;
add_to_timer:
  if timer_status != 1
  {
    return
  }
  if movement_reminder_status = 1
  {
	movement_timer := movement_timer + 1
	output_reminder_time := format_seconds(movement_timer)
	GuiControl, MainGui:, control_movement_reminder_elapsed, %output_reminder_time%
  }
  elapsed_time := elapsed_time + 1 	
  output_time := format_seconds(elapsed_time)
  GuiControl, MainGui:, control_elapsed_time, %output_time%
return

;;
; Checks for a running task and blinks the icon if one is not running (To remind user that nothing is being tracked)
;;
check_for_running_timer:
  if timer_status = 1
  {
   return
  }

  Loop 5
  {
    Gui, MainGui:Flash
    Sleep 500  ; It's quite sensitive to this value; altering it may change the behavior in unexpected ways.
  }
return

;;
; Checks if it's time to remind to movement_reminder
;;
check_for_movement_reminder:
  if timer_status != 1
  {
   return
  }
  if movement_reminder_status != 1
  {
    return
  }
  if (movement_timer >= movement_reminder)
  {
	Msgbox, 0 , , It's time to get up and move!
	movement_timer = 0
	movement_reminder_status = %movement_reminder_status_default%
  }
return

;;
; Gets date values and returns them
;;
get_date_values(byRef year, byRef month, byRef day, byRef day_week)
{
  FormatTime, year,,yyyy
  FormatTime, day,,d
  FormatTime, day_week,,ddd
  FormatTime, month,,MMM
}

;;
; Formats time to correct hour:min:sec: format
;;
format_seconds(sec)
{
  time = 19990101  ; *Midnight* of an arbitrary date.
  time += %sec%, seconds
  FormatTime, mmss, %time%, mm:ss
  return sec//3600 ":" mmss
}

;;
; A helper func to handle most summarization tasks
;;
summarize_tasks(number_of_days, type)
{
  get_date_values(year, month, day, day_week)

  file_list := []
  file_list := get_last_task_files(number_of_days, type)
  
  task_object := []
  task_object := parse_and_summarize(file_list)
  
  display_summary(task_object, type, month, year)
}

;;
; Summarizes the selected month/year tasks
;;
summarize_user_selection(selected_month, selected_year, type)
{
  
  if(selected_month != "")
  {
    month_hash := {1:"Jan", 2:"Feb", 3:"Mar", 4:"Apr", 5:"May", 6:"Jun", 7:"Jul", 8:"Aug", 9:"Sep", 10:"Oct", 11:"Nov", 12:"Dec"}
    month_name := month_hash[selected_month]
    
    file_path = C:\Users\%euid%\Documents\Task Tracker\%selected_year%\%month_name%\
  }
  else
  {
    file_path = C:\Users\%euid%\Documents\Task Tracker\%selected_year%\
  }
  file_list := []
  
  loop, files, %file_path%*.*, R
  {
    full_path = %A_LoopFileDir%\%A_LoopFileName%
    file_list.Push(full_path)
  }

  task_object := []
  task_object := parse_and_summarize(file_list)
  
  display_summary(task_object, type, month_name, selected_year)
}

;;
; Parses the given files and summarizes the tasks in them. Returns hash summary
;;
parse_and_summarize(file_list)
{
  task_summary := {}
  task_comments := {}
  task_format := {}
  first_date =
  last_date =
  outer_index = 0

  loop, % file_list.MaxIndex()
  {
    outer_index := outer_index + 1
    loop, Read, % file_list[outer_index]
    {
      StringSplit, split_line, A_LoopReadLine, "|"
      task = %split_line1%
      StringLower, task_lower, task
      if task_summary.HasKey(task_lower)
      {
        task_summary[task_lower] := task_summary[task_lower] + split_line2
        task_comments[task_lower] := task_comments[task_lower] . " " . split_line4
      }
      else
      {
        task_summary[task_lower] := split_line2
        task_comments[task_lower] := split_line4
        task_format[task_lower] := task
      }
      if outer_index = 1
      {
        last_date := get_pretty_date_from_filename(file_list[outer_index])
      }
      if(outer_index = file_list.MaxIndex())
      {
        first_date := get_pretty_date_from_filename(file_list[outer_index])
      }
    }
  }
  
  summary_object := [task_summary, task_comments, task_format, first_date, last_date]
  return summary_object
}

;;
; Pulls each file for the last week and creates a summary for each day
;;
summarize_each_day_of_week(num_day_week, type)
{
  file_list := get_last_task_files(num_day_week, type)
  outer_index = 0
  object_array := []

  ;; TODO: FINISH THIS
  loop, % file_list.MaxIndex()
  { 
    file := % file_list[outer_index]
    outer_index := outer_index + 1
    task_object := []
    task_object := parse_and_summarize(file)
    pretty_file := get_pretty_date_from_filename(file)
    msgbox % task_object[1]
    task_object[1] := pretty_file task_object[1]
    msgbox % task_object[1]
  }
}

;;
; Displays the summary to the user
;;
display_summary(task_object, type, month, year)
{
  task_summary := task_object[1]
  task_comments := task_object[2]
  task_format := task_object[3]
  first_date := task_object[4]
  last_date := task_object[5]
  
  Run, notepad.exe,,,notepad_pid
  WinWaitActive ahk_pid %notepad_pid%
  
  if type = day
  {
    ControlSend, Edit1, %first_date% Task Summary`r, ahk_pid %notepad_pid%
  }
  else If type = week
  {
    ControlSend, Edit1, %first_date% - %last_date% Task Summary`r`r, ahk_pid %notepad_pid%
  }
  else if type = month
  {
    ControlSend, Edit1, %month% %year% Task Summary`r, ahk_pid %notepad_pid%
  }
  else if type = year
  {
    ControlSend, Edit1, %year% Task Summary`r, ahk_pid %notepad_pid%
  }
  
  ControlSend, Edit1, Task`tTime`tComments`r, ahk_pid %notepad_pid%    
  for task_lower, total_time in task_summary
  {
    formatted_time := format_seconds(total_time)
    formatted_task := task_format[task_lower]
    comments := task_comments[task_lower]
    ControlSend, Edit1, %formatted_task% `t %formatted_time% `t %comments%`r, ahk_pid %notepad_pid%
  }
}

;;
; Gets the task files needed based on the summary type. Returns array of filenames
;;
get_last_task_files(number, type)
{
  total_lines := get_file_lines()
  files := []
  file_index = 0
  loop, %number%
  {
    FileReadLine, line, %db_file%, (total_lines - file_index)
    if ErrorLevel
      break
    file_index := file_index + 1
    if is_out_of_bounds(line, type)
      continue
    files.Push(line)
  }

  return files
}

;;
; Gets the number of lines in the db file
;;
get_file_lines()
{
  loop, Read, %db_file%
  {
    file_lines = %A_Index%
  }
  return file_lines
}

;;
; Pulls a prettily formatted date from the given task filename
;;
get_pretty_date_from_filename(name)
{
  StringSplit, split_name, name, `\
  month := SubStr(split_name7, 1, 3)
  day := SubStr(split_name8, 1, 2)
  day := RegExReplace(day, "," , "")
  return month . " " . day
}

;;
; Checks whether the file is out of the summary type boundary
;;
is_out_of_bounds(file, type)
{
  get_date_values(year, month, day, day_week)
  StringSplit, split_file, file, `,
  if(type = "day")
  {
    RegexMatch(split_file1, "([0-9]+)$", file_day)
    if(file_day != day)
    {
      return True
    }
    else
    {
      return False

    }
  }
  else if(type = "week" or type = "week_by_day")
  {
    num_day_week := week_day_table[day_week]
    RegexMatch(split_file1, "([0-9]+)$", file_day)
    if(file_day < (day - (num_day_week - 1)))
    {
      return True
    }
    else
    {
      return False
    }
  }
  else if(type = "month")
  {
    StringSplit, split_again, split_file1, `\
    file_mon = %split_again7%
    if(file_mon != month)
    {
      return True
    }
    else
    {
      return False
    }
  }
}

;;
; Appends time to task file
;;
append_time_to_task(task, comments, elapsed_time)
{
  get_date_values(year, month, day, day_week)

  check_tracker_folders(year, month, day, euid)

  file_path = C:\Users\%euid%\Documents\Task Tracker\%year%\%month%\%day%, %day_week% Tasks.txt

  StringLower, task_lowercase, task

  if !FileExist(file_path)
  {
    elapsed_hrs := calculate_hours(elapsed_time)
    comments := RegExReplace(comments, "`r|`n" , "; ")
    comments := RegExReplace(comments, "([^;])$" , "$1;")
    FileAppend, %task%|%elapsed_time%|%elapsed_hrs%|%comments%`r`n, %file_path%
    save_file_record_to_db(file_path)
    return
  }

  found_task =
  new_file_contents =

  loop, Read, %file_path%
  {
    StringSplit, split_line, A_LoopReadLine, "|"
    StringLower, file_task, split_line1
    if file_task = %task%
    {
      ; Task already exists, appending time ;
      found_task = 1
      new_elapsed_time := split_line2 + elapsed_time
      new_elapsed_hrs := calculate_hours(new_elapsed_time)
      Loop
      {
        If InStr(comments, "`n")
        {
          comments := RegExReplace(comments, "`r|`n" , "; ")
          comments := RegExReplace(comments, "([^;])$" , "$1;")
          continue
        }
        break
      }
      comments := split_line4 . comments . "`r`n"
      new_file_contents .= task . "|" . new_elapsed_time . "|" . new_elapsed_hrs . "|" . comments
    }
    else
    {
      ; Line in file does not contain current task. Re-building file;
      new_file_contents := new_file_contents . A_LoopReadLine . "`r`n"
    }
  }

  if found_task != 1
  {
    ; Task was not appended, adding to end of file ;
    elapsed_hrs := calculate_hours(elapsed_time)
    new_file_contents .= task . "|" . elapsed_time . "|" . elapsed_hrs . "|" . comments . "`r`n"
  }

  FileDelete %file_path%
  FileAppend, %new_file_contents%, %file_path%

  return
}

;;
; Calculates elapsed time in hours and returns result
;;
calculate_hours(elapsed_sec)
{
  calculated_hrs := (elapsed_sec  / 60) / 60
  return Round(calculated_hrs, 1)
}

;;
; Saves the file record to a 'DB' file to ease in summarizing tasks
;;
save_file_record_to_db(file)
{
  FileAppend, %file%`r`n, %db_file%
}

;;
; Checks existence of / creates the directory structure needed for task files
;;
check_tracker_folders(year, month, day, euid)
{
  if !InStr(FileExist("C:\Users\" . euid . "\Documents\Task Tracker"), "D")
  {
    FileCreateDir, C:\Users\%euid%\Documents\Task Tracker
  }
  if !InStr(FileExist("C:\Users\" . euid . "\Documents\Task Tracker\" . year), "D")
  {
    FileCreateDir, C:\Users\%euid%\Documents\Task Tracker\%year%
  }
  if !InStr(FileExist("C:\Users\" . euid . "\Documents\Task Tracker\" . year . "\" . month), "D")
  {
    FileCreateDir, C:\Users\%euid%\Documents\Task Tracker\%year%\%month%
  }
  if !InStr(FileExist("C:\Users\" . euid . "\Documents\Task Tracker\bin\"), "D")
  {
    FileCreateDir, C:\Users\%euid%\Documents\Task Tracker\bin
  }
}

; Shamelessly stolen from AHK forums
;-------------------------------------------------------------------------------
Gui_AddPicture(Options, Colour) { ; hex RGB with spaces
;-------------------------------------------------------------------------------
    FileName := A_Temp "\" Colour ".bmp"
    Handle := DllCall("CreateFile", "Str", FileName, "Int", 0x40000000
        , "Int", 0, "Int", 0, "Int", 4, "Int", 0, "Int", 0)

    ;---------------------------------------------------------------------------
    Picture =
    ;---------------------------------------------------------------------------
        ( Join LTrim
            42 4D 3A 00 | 00 00 00 00 | 00 00 36 00 | 00 00 28 00
            00 00 01 00 | 00 00 01 00 | 00 00 01 00 | 18 00 00 00
            00 00 04 00 | 00 00 00 00 | 00 00 00 00 | 00 00 00 00
            00 00 00 00 | 00 00
        )

    Picture .= SubStr(Colour, 7, 2)
            .  SubStr(Colour, 4, 2)
            .  SubStr(Colour, 1, 2) "00"
    StringReplace, Picture, Picture, |,, All
    StringReplace, Picture, Picture, %A_Space%,, All

    Loop, % StrLen(Picture) // 2 {
        StringLeft, Hex, Picture, 2
        StringTrimLeft, Picture, Picture, 2
        DllCall("WriteFile", "Int", Handle, "CharP", "0x" Hex
            , "Int", 1, "IntP", BytesWritten, "Int", 0)
    }
    DllCall("CloseHandle", "Int", Handle)
    Gui, MainGui:Add, Picture, %Options%, %FileName%
    FileDelete, %FileName%
}
